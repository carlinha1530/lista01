/**
 *
 * A resolucao da lista eh individual. Caso seja identificada "cola", 
 * todos os envolvidos terao a nota da lista zerada.
 * 
 *
 */
public class Lista01 {

	/**
	 * 01 OK
	 * Encontre o maior elemento do vetor e o retorne. 
	 */
	public static int maiorElementoVetor(int[] vetor) {
		int maior = vetor[0];
		    for (int i = 0; i < vetor.length; i++) {
				if(maior < vetor[i])
					maior = vetor[i];
			}
		return maior;
	}

	/**
	 * 02
	 * Encontre o maior elemento da matriz e o retorne. 
	 */
	public static int maiorElementoMatriz(int[][] matriz) {
		 int maiorE = 0;
			for (int i = 0; i < matriz.length; i++) {
				for (int j = 0; j < matriz[i].length; j++) {
					if(maiorE < matriz [i][j]){
						maiorE = matriz [i][j];
				}
			}
		}
	return maiorE;
	}

	/**
	 * 03
	 * Defina um vetor com o valor do primeiro parametro e inicialize
	 * todas as posicoes deste vetor com o segundo parametro.
	 * 

	 */
	public static int[] criaVetor(int tamanhoVetor, int valorInicial) {
		   	int [] vet = new int[tamanhoVetor];	
		   	for (int i = 0; i < vet.length; i++) {
			 vet[i] = valorInicial;
			}
		return vet;
	}

	/**
	 * 04 OK
	 * Dado o vetor de registros do tipo pessoa, retorna o nome daquela 
	 * que tem a maior idade (o mais velho).
	 * 
	 */
	public static String recuperarNomeMaisVelha(Pessoa[] pessoas) {
		String Nome = null;
		int Idade = 0;
		    for (int i = 0; i < pessoas.length; i++) {
				if(Idade < pessoas[i].idade){
					Idade = pessoas[i].idade;
					Nome = pessoas[i].nome;
				}
			}
		return Nome;
	}

	/**
	 * 05 OK
	 * Dado o vetor de registros do tipo pessoa, calcular a media das 
	 * idades de todas as pessoas do conjunto.
	 * 
	 */
	public static double calcularMediaIdade(Pessoa[] pessoas) {
		  double media = 0;
		  double soma = 0;
		     for (int i = 0; i < pessoas.length; i++) {
				  soma = soma + pessoas[i].idade;	
		     }
		     media = soma / pessoas.length;
		return media;
	}

	/**
	 * 06 OK
	 * Verifica se o nome buscado existe no vetor de pessoas passado como
	 * argumento de entrada. 
	 * 
	 * Dica: pesquise pela funcao equals().
	 * 
	 */
	public static boolean existePessoaVetor(String nomeBuscado, Pessoa[] pessoas) {
		   		    
		for (int i = 0; i < pessoas.length; i++) {
			if (nomeBuscado.equals(pessoas[i].nome))
				return true;
		}
		return false;
	}

}
