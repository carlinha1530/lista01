import java.util.Arrays;


/**
 * 
 * Classe de testes. Nao altere nada neste arquivo.
 *
 */
public class Lista01Testes {

	public static void main(String[] args) {

		if (
				testesMaiorElementoVetor() & 
				testesMaiorElementoMatriz() &
				testesCriarVetor() &
				testesPessoaMaisVelha() &
				testesMediaIdade() &
				testesExistePessoa() &
				true
			) {
			System.out.println("Todos os testes executaram corretamente!");
		}
		
	}

	private static boolean testesMaiorElementoVetor() {		
		return 
				teste01() & 
				teste02() & 
				teste03() & 
				teste04() & 
				true;
	}

	private static boolean teste01() {
		int maior = Lista01.maiorElementoVetor(new int[]{10, 20, 30});
		
		if (maior != 30) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste02() {
		int maior = Lista01.maiorElementoVetor(new int[]{10});
		
		if (maior != 10) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}
	
	private static boolean teste03() {
		int maior = Lista01.maiorElementoVetor(new int[]{50, 20, 30, 45, 49});
		
		if (maior != 50) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}
	
	private static boolean teste04() {
		int maior = Lista01.maiorElementoVetor(new int[]{-1, -2, -3, -5});
		
		if (maior != -1) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}
	
	private static boolean testesMaiorElementoMatriz() {
		return 
				teste05() & 
				teste06() &
				teste07() &
				teste08() &
				teste09() &
				true;

	}

	private static boolean teste05() {
		int maior = Lista01.maiorElementoMatriz(new int[][]{{1,2},{3,4}});
		
		if (maior != 4) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste06() {
		int maior = Lista01.maiorElementoMatriz(new int[][]{{10},{40}});
		
		if (maior != 40) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste07() {
		int maior = Lista01.maiorElementoMatriz(new int[][]{{1,2,3},{3,4,5},{6,7,8}});
		
		if (maior != 8) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}
	
	private static boolean teste08() {
		int maior = Lista01.maiorElementoMatriz(new int[][]{{1,2,3,4},{3,4,5,6}});
		
		if (maior != 6) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste09() {
		int maior = Lista01.maiorElementoMatriz(new int[][]{{-1,2},{3,-4}});
		
		if (maior != 3) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean testesCriarVetor() {
		return 
		teste10() &
		teste11() &
		teste12() &
		teste13() &
		true;
	}

	private static boolean teste10() {
		int[] vetor = Lista01.criaVetor(3, 2);
		
		if (vetor == null) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (vetor.length != 3) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (!Arrays.equals(vetor, new int[]{2,2,2})) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste11() {
		int[] vetor = Lista01.criaVetor(1, 10);
		
		if (vetor == null) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (vetor.length != 1) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (!Arrays.equals(vetor, new int[]{10})) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}
	
	private static boolean teste12() {
		int[] vetor = Lista01.criaVetor(5, 1);
		
		if (vetor == null) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (vetor.length != 5) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (!Arrays.equals(vetor, new int[]{1,1,1,1,1})) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}
	
	private static boolean teste13() {
		int[] vetor = Lista01.criaVetor(3, 3);
		
		if (vetor == null) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (vetor.length != 3) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (!Arrays.equals(vetor, new int[]{3,3,3})) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}
	
	private static boolean testesPessoaMaisVelha() {
		return 
		teste14() &
		teste15() &
		teste16() &
		teste17() &
		true;
	}

	private static boolean teste14() {
		String nome = Lista01.recuperarNomeMaisVelha(new Pessoa[]{
				new Pessoa("Joao", 10),
				new Pessoa("Maria", 20),
				new Pessoa("Ricardo", 30)
		});
		
		if (nome == null) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (!nome.equals("Ricardo")) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste15() {
		String nome = Lista01.recuperarNomeMaisVelha(new Pessoa[]{
				new Pessoa("Joao", 10)
		});
		
		if (nome == null) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (!nome.equals("Joao")) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste16() {
		String nome = Lista01.recuperarNomeMaisVelha(new Pessoa[]{
				new Pessoa("Marcelo", 40),
				new Pessoa("Joao", 10),
				new Pessoa("Maria", 20),
				new Pessoa("Ricardo", 30)
		});
		
		if (nome == null) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (!nome.equals("Marcelo")) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste17() {
		String nome = Lista01.recuperarNomeMaisVelha(new Pessoa[]{
				new Pessoa("Joao", 10),
				new Pessoa("Maria", 20)
		});
		
		if (nome == null) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}

		if (!nome.equals("Maria")) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean testesMediaIdade() {
		return 
		teste18() &
		teste19() &
		teste20() &
		teste21() &
		true;
	}

	private static boolean teste18() {
		double media = Lista01.calcularMediaIdade(new Pessoa[]{
				new Pessoa("Joao", 10),
				new Pessoa("Maria", 20),
				new Pessoa("Ricardo", 30)
		});
		
		if (media != 20) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste19() {
		double media = Lista01.calcularMediaIdade(new Pessoa[]{
				new Pessoa("Joao", 10)
		});
		
		if (media != 10) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste20() {
		double media = Lista01.calcularMediaIdade(new Pessoa[]{
				new Pessoa("Marcelo", 40),
				new Pessoa("Joao", 10),
				new Pessoa("Maria", 20),
				new Pessoa("Ricardo", 30)
		});
		
		if (media != 25) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste21() {
		double media = Lista01.calcularMediaIdade(new Pessoa[]{
				new Pessoa("Joao", 10),
				new Pessoa("Maria", 20)
		});
		
		if (media != 15) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}
	
	private static boolean testesExistePessoa() {
		return 
		teste22() &
		teste23() &
		teste24() &
		teste25() &
		teste26() &
		true;

	}

	private static boolean teste22() {
		Pessoa[] parametros = new Pessoa[2];
		parametros[0] = new Pessoa("Joao", 10);
		parametros[1] = new Pessoa("Maria", 20);
		
		boolean existe = Lista01.existePessoaVetor("Joao", parametros);
		
		if (existe == false) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste23() {
		Pessoa[] parametros = new Pessoa[2];
		parametros[0] = new Pessoa("Joao", 10);
		parametros[1] = new Pessoa("Maria", 20);
		
		boolean existe = Lista01.existePessoaVetor("Francisco", parametros);
		
		if (existe == true) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste24() {
		Pessoa[] parametros = new Pessoa[1];
		parametros[0] = new Pessoa("Joao", 10);
		
		boolean existe = Lista01.existePessoaVetor("Francisco", parametros);
		
		if (existe == true) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

	private static boolean teste25() {
		Pessoa[] parametros = new Pessoa[1];
		parametros[0] = new Pessoa("Joao", 10);
		
		boolean existe = Lista01.existePessoaVetor("Joao", parametros);
		
		if (existe == false) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}
	
	private static boolean teste26() {
		Pessoa[] parametros = new Pessoa[4];
		parametros[0] = new Pessoa("Joao", 10);
		parametros[1] = new Pessoa("Maria", 20);
		parametros[2] = new Pessoa("Ricardo", 30);
		parametros[3] = new Pessoa("Marta", 35);
		
		boolean existe = Lista01.existePessoaVetor("Marta", parametros);
		
		if (existe == false) {
			System.err.println("Erro em " + Thread.currentThread().getStackTrace()[1].getMethodName());
			return false;
		}
		
		return true;
	}

}
